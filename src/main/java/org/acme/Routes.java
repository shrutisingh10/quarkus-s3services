package org.acme;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.aws.s3.S3Constants;


public class Routes extends RouteBuilder {

    public Routes() {
    }

    /**
     * Route for Quarkus s3 camel extension
     * Replace bucketName, accesKey, secretKey
     * Uncomment below code to run s3 camel extension operation
     */
    @Override
    public void configure() throws Exception {
//
//        //Listing all objects of specified s3 bucket
//        from("timer://foo?repeatCount=1")
//                .to("aws-s3://test-bucket?accessKey=test-key&secretKey=test-secret&operation=listObjects")
//                .log("${body}");
//
//        //Uploading file to specified s3 bucket
//        from("file:src/main/resources?fileName=abcd.txt")
//                .setHeader(S3Constants.KEY, constant("testFile"))
//                .to("aws-s3://test-bucket?accessKey=test-key&secretKey=test-secret")
//                .log("${body}");
//
//        //Getting download link of specific object of s3 bucket by objectKey
//        from("timer://foo?repeatCount=1")
//                .process(exchange -> exchange.getIn().setHeader(S3Constants.KEY, "camelKey"))
//                .to("aws-s3://test-bucket?accessKey=test-key&secretKey=test-secret&operation=downloadLink")
//                .log("${body}");

    }
}
