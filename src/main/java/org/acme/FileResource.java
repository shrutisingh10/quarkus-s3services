package org.acme;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

@Path("/s3")
public class FileResource{

    @Inject
    S3Service s3Service;

    @POST
    @Path("upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(@MultipartForm FormData formData) {
        if (formData.fileName == null || formData.fileName.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        if (formData.mimeType == null || formData.mimeType.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return s3Service.upload(formData);
    }

    @GET
    @Path("download/{objectKey}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response downloadFile(@PathParam("objectKey") String objectKey) {
        return s3Service.download(objectKey);
    }

    @GET
    public List<FileObject> listFiles() {
      return s3Service.list();
    }
}
