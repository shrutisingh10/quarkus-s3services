package org.acme;

import java.io.InputStream;
import javax.ws.rs.FormParam;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.providers.multipart.PartType;

public class FormData {

    /** Represents the file data.
     */
    @FormParam("file")
    @PartType(MediaType.APPLICATION_OCTET_STREAM)
    public InputStream data;

    /** Represents the name of the file.
     */
    @FormParam("filename")
    @PartType(MediaType.TEXT_PLAIN)
    public String fileName;

    /** Represents the media type of the file.
     */
    @FormParam("mimetype")
    @PartType(MediaType.TEXT_PLAIN)
    public String mimeType;

}
