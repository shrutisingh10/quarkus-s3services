package org.acme;

import javax.ws.rs.core.Response;
import java.util.List;

public interface S3Service {

    List<FileObject> list();

    Response upload(FormData formData);

    Response download(String objectKey);
}
